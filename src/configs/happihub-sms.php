<?php

return [
    'server' => env('HAPPIHUB_SMS_SERVER', 'https://sms.happihub.com'),
    'app_key' => env('HAPPIHUB_SMS_APP_KEY', ''),
    'app_secret' => env('HAPPIHUB_SMS_APP_SECRET', '')
];
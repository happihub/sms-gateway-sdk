<?php

namespace Happihub\Sms\Providers;

use Happihub\Sms\HappihubSms;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider {
    public function register()
    {
        $this->publishes([
            __DIR__ . '/../configs/happihub-sms.php' => config_path('happihub-sms.php')
        ], 'happihub-sms');
        $this->mergeConfigFrom(__DIR__ . '/../configs/happihub-sms.php', 'happihub-sms');

        $this->app->bind(HappihubSms::class, function (){
            return new HappihubSms(
                config('happihub-sms.app_key'),
                config('happihub-sms.app_secret'),
                config('happihub-sms.app_server'),
            );
        });
    }

    public function provides()
    {
        return [HappihubSms::class];
    }
}
<?php

namespace Happihub\Sms;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class HappihubSms
{
    /**
     * @var null|string
     */
    protected $server;

    /**
     * @var null|string
     */
    protected $app_key;

    /**
     * @var null|string
     */
    protected $app_secret;

    public function __construct($app_key = null, $app_secret = null, $server = null)
    {
        $this->server = $server ?: rtrim(config('happihub-sms.server'), '/');
        $this->app_key = $app_key ?: config('happihub-sms.app_key');
        $this->app_secret = $app_secret ?: config('happihub-sms.app_secret');
    }

    public function send($target, $message)
    {
        return $this->request()
            ->post('message', [
                'phone' => $target,
                'body' => $message
            ]);
    }

    /**
     * @return \Illuminate\Http\Client\PendingRequest
     */
    protected function request()
    {
        return Http::baseUrl("{$this->server}/api/customer")
            ->withHeaders($this->credentials())
            ->asJson();
    }

    /**
     * @return array
     */
    protected function credentials()
    {
        return [
            'app-key' => $this->app_key,
            'app-secret' => $this->app_secret
        ];
    }
}